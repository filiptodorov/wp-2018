package mk.ukim.finki.wp.organizeme.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Riste Stojanov
 */
@Controller
public class HelloWorldController {

    @RequestMapping(value = "/hello",
            method = RequestMethod.GET)
    public String helloWorld() {

        // not rendered
        String text = "Hello World";

        return "hello-template";

    }

    @RequestMapping(value = "/hello1",
            method = RequestMethod.GET)
    public String helloWorldWithoutTemplate() {

        // not rendered
        String text = "Hello World";

        return "hello-template";

    }

    @RequestMapping(value = "/hello2",
            method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView helloWorldModel() {

        // Can be rendered in the template
        String text = "Hello World";

        ModelAndView result = new ModelAndView("hello-template.html");

        result.addObject("text", text);

        return result;

    }

    @RequestMapping(value = "/hello3",
            method = RequestMethod.GET)
    public String helloWorldRequestAttribute(
            HttpServletRequest request) {

        // not rendered
        String text = "Hello World";

        request.setAttribute("text", text);
        return "hello-template";

    }


}
