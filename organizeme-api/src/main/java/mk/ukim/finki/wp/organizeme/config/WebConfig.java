package mk.ukim.finki.wp.organizeme.config;

import mk.ukim.finki.wp.organizeme.web.converters.MyStringToDurationConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Riste Stojanov
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        // Amount

        //ZonedDateTime
        registry.addConverter(new MyStringToDurationConverter());
    }

}
