package mk.ukim.finki.wp.organizeme.model.exceptions;

/**
 * @author Riste Stojanov
 */
public class DuplicateTaskException extends Exception {
}
