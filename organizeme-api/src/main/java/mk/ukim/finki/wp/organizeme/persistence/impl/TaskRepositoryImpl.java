package mk.ukim.finki.wp.organizeme.persistence.impl;

import mk.ukim.finki.wp.organizeme.model.Activity;
import mk.ukim.finki.wp.organizeme.model.Client;
import mk.ukim.finki.wp.organizeme.model.Project;
import mk.ukim.finki.wp.organizeme.model.Task;
import mk.ukim.finki.wp.organizeme.model.exceptions.TaskNotFoundException;
import mk.ukim.finki.wp.organizeme.persistence.TaskRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * @author Riste Stojanov
 */
@Profile("in-memory-db")
@Repository
public class TaskRepositoryImpl implements TaskRepository {

    private static List<Task> tasks;

    @PostConstruct
    public void init() {
        Client finki = new Client();
        finki.id = 1L;
        finki.name = "FINKI";

        Project wp = new Project();
        wp.id = 1L;
        wp.name = "WP";
        wp.client = finki;

        Activity a1 = new Activity();
        a1.date = LocalDate.now();
        a1.from = LocalTime.now().minusHours(1).minusMinutes(3).minusSeconds(3);
        a1.to = a1.from.plusMinutes(40);

        Activity a2 = new Activity();
        a2.date = a1.date;
        a2.from = a1.to;

        Task t1 = new Task();
        t1.title = "T1";
        t1.project = wp;
        t1.totalTime = Duration.ofHours(1).plusMinutes(3).plusSeconds(3);
        t1.activity = Stream
          .of(a1, a2)
          .collect(toList());

        tasks = Stream.of(t1, t1, t1)
          .collect(toList());
    }


    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public Task getTaskById(int index) {
        if (index >= tasks.size()) {
            throw new TaskNotFoundException();
        }
        return tasks.get(index);
    }


    @Override
    public Task save(Task task) {
        task.id = tasks.size();
        tasks.add(task);
        return tasks.get(tasks.size() - 1);
    }


    @Override
    public Task deleteById(int index) {
        if (index >= tasks.size() || index < 0) {
            throw new TaskNotFoundException();
        }
        return tasks.remove(index);
    }

    @Override
    public Task update(int index, String title) {
        Task task = getTaskById(index);
        task.title = title;
        return task;
    }
}
