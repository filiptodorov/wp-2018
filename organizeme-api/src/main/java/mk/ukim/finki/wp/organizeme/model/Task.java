package mk.ukim.finki.wp.organizeme.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.DurationDeserializer;
import mk.ukim.finki.wp.organizeme.web.converters.MyStringToDurationConverter;
import org.springframework.data.convert.Jsr310Converters;

import javax.persistence.*;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author Riste Stojanov
 */
@Entity
public class Task {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    public Integer id;

    public String title;

    public boolean done;

    @ManyToOne
    public Project project;

    public Duration totalTime;

    @OneToMany(mappedBy = "task", fetch = FetchType.EAGER)
    public List<Activity> activity = new ArrayList<>();

    @JsonIgnore
    @ManyToMany(mappedBy = "tasks")
    public List<User> users = new ArrayList<>();
}
