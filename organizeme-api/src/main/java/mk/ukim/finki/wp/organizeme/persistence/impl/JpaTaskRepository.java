package mk.ukim.finki.wp.organizeme.persistence.impl;

import mk.ukim.finki.wp.organizeme.model.Task;
import mk.ukim.finki.wp.organizeme.persistence.TaskRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author Riste Stojanov
 */
@Profile("jpa-db")
public interface JpaTaskRepository extends
  Repository<Task, Integer>, TaskRepository {

    @Override
    @Query("select t from Task t where id=:id")
    Task getTaskById(@Param("id") int id);

    @Override
    default Task update(int id, String title) {
        int updated = updateTaskTitle(id, title);
        return getTaskById(id);
    }


    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Task t set t.title=:title where t.id=:id")
    int updateTaskTitle(@Param("id") int id, @Param("title") String title);

    Optional<Task> findById(Integer id);
}
