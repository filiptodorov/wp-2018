package mk.ukim.finki.wp.organizeme.model;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author Riste Stojanov
 */
@Entity
public class Project {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    public Long  id;

    public String name;

    @ManyToOne
    public Client client;
}
