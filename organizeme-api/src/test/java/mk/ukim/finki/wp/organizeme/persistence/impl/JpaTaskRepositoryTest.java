package mk.ukim.finki.wp.organizeme.persistence.impl;

import mk.ukim.finki.wp.organizeme.model.Task;
import mk.ukim.finki.wp.organizeme.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Riste Stojanov
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class JpaTaskRepositoryTest {

    public static final String NEW_TITLE = "new title";
    @Autowired
    JpaTaskRepository jpaTaskRepository;

    @Autowired
    JpaUserRepository userRepository;

    @PersistenceContext
    EntityManager em;

    @Test
    public void update() throws Exception {
        Task task = new Task();
        task.title = "old title";
        Task savedTask = jpaTaskRepository.save(task);

        Assert.assertNotNull(savedTask.id);

        Task updatedTask = jpaTaskRepository.update(savedTask.id, NEW_TITLE);

        // This makes the test pass, but does not fix the causing problem
        // em.refresh(updatedTask);

        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(NEW_TITLE, updatedTask.title);

        Task loadedTask = jpaTaskRepository.getTaskById(updatedTask.id);
        Assert.assertNotNull(loadedTask);
        Assert.assertEquals(loadedTask.title, NEW_TITLE);

    }

    @Test
    public void referencingShowcase() {

        Task task = new Task();
        task.title = "old title";
        Task savedTask = jpaTaskRepository.save(task);

        em.flush();
        em.clear();

        User u = new User();
        u.firstName = "Riste";
        u.lastName = "Stojanov";
        u.username = "ristes";

        u = userRepository.save(u);
        em.flush();
        em.clear();

        u.tasks.add(savedTask);

        u = userRepository.save(u);
        em.flush();
        em.clear();

        u = userRepository.findById(u.username).get();

        Assert.assertNotNull(u.tasks);
        Assert.assertEquals(u.tasks.size(), 1);
        Assert.assertNotNull(u.tasks.get(0));


        Assert.assertNotNull(savedTask.id);

        savedTask.users.add(u);
        savedTask = jpaTaskRepository.save(savedTask);

        em.flush();
        em.clear();

        savedTask = jpaTaskRepository.findById(savedTask.id)
          .get();

        Assert.assertNotNull(savedTask.users);
        Assert.assertEquals(savedTask.users.size(), 1);
        Assert.assertNotNull(savedTask.users.get(0));


    }

}