# Spring MVC
Spring Boot is based on MVC concept for web development. Spring MVC is the original web framework built on the Servlet API. It is build on the popular MVC design pattern. MVC (Model-View-Controller), as software architecture pattern, separates application into three areas: model, view, and controller. The model represents a Java object which holds the data. The view provides visualization of the data that the model contains. The controller controls the data flow into model object and updates the view whenever data changes. It keeps view and model separate.

# Controllers in Spring Boot

`@Controller` annotation indicates that the annotated class is a controller. It is a specialization of @Component and is autodetected through classpath scanning. It is typically used in combination with annotated handler methods based on the @RequestMapping annotation. In the most of the cases, the views returned by the controller should implement a ViewTemplate engine like JSP or Thymeleaf, in order to bind data exchange between Views and Controllers.

## Let's create a simple @Controller
In order to create simple controller in Spring project, create a new subpackage in base package and call it `web`. In `web` subpackage create additional subpackage and call it `controllers`. In `controllers` add new Java class and call it `HelloWorldController.java`. 
```java
package mk.ukim.finki.wp.organizeme.web.controllers;
@Controller
public class HelloWorldController {
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	@ResponseBody
    public String helloWorld() {
        // not rendered
        String text = "Hello World";
        return "hello-template";
    }
}
```
The method `helloWorld()` provides an API endpoint which can be invoked with GET method by using the following URL: `http://localhost:8080/hello`, assuming that the application server is started on port 8080. The result of API invocation is a HTML page which renders the returned string "hello-template". `@RequestMapping(value = "/hello", method = RequestMethod.GET)` provides mapping of the path `'/hello'` as GET request. 


### Model and view
`@ModelAndView` is an interface which provides passing values to a view. This interface allows us to pass all the information required by Spring MVC in one return. The HTML template, which should be rendered as response body, has to be placed in `static` folder placed in the `resources`. Additional attributes can be added to interface ModelAndView by using the method `addObject`.
```java
@RequestMapping(value = "/hello2",
            method = RequestMethod.GET)
    public ModelAndView helloWorldModel() {

        // Can be rendered in the template
        String text = "Hello World";

        ModelAndView result = new ModelAndView("hello-template");

        result.addObject("text", text);

        return result;

    }
```


# REST Controllers in Spring Boot
In order to create Restful web services, Spring Boot provides sibling convenience annotation named as `@RestController` which uses the similar concepts as `@Controller` annotation, but serving the requested data in REST-oriented format like JSON. More about REST services, you can read in Chapter 4.16 in the book `Spring Boot in Action, 4th Edition`.
`@RestControllers` can handle requests for all HTTP methods, including the four primary REST methods: **GET**, **PUT**, **DELETE**, **POST** and **PATCH**. 

## Create the first `@RestController`
In order to create C(reate)R(ead)U(pdate)D(elete) RESTful API endpoints, which are able to manage the Task entity defined in `Task.java`, we will create a new class `TaskResource.java`. In Spring Boot, RESTful controllers are annotated with `@RestController`.

```java
@RestController
@RequestMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
public class TaskResource {
	
}
```
The initialization of the list that holds the instances of the class `Task`, can be done in the method init which is annotated with @PostConstruct. The PostConstruct annotation is used on a method that needs to be executed after dependency injection is done to perform any initialization. This method MUST be invoked before the class is put into service. 

```java@PostConstruct
    public void init() {
        Client finki = new Client();
        finki.id = 1L;
        finki.name = "FINKI";

        Project wp = new Project();
        wp.id = 1L;
        wp.name = "WP";
        wp.client = finki;

        Activity a1 = new Activity();
        a1.date = LocalDate.now();
        a1.from = LocalTime.now().minusHours(1).minusMinutes(3).minusSeconds(3);
        a1.to = a1.from.plusMinutes(40);

        Activity a2 = new Activity();
        a2.date = a1.date;
        a2.from = a1.to;

        Task t1 = new Task();
        t1.title = "T1";
        t1.project = wp;
        t1.totalTime = Duration.ofHours(1).plusMinutes(3).plusSeconds(3);
        t1.activity = Stream
          .of(a1, a2)
          .collect(toList());

        TaskResource.tasks = Stream.of(t1, t1, t1)
          .collect(toList());
    }
```	



## Request Mapping

 `@RequestMapping` is an annotation for mapping web requests onto methods in request-handling classes with flexible method signatures. It can be applied to the controller class as well as methods.  Used in combination with `@RestController`, it provides definition of the base path of the REST resource, in this showcase ***/tasks***, as defined in the `value` parameter. The parameter `produces` defines the media types produced by the controller, by default `'application/json'`.
 In order to define REST API endpoints, we have to write Java methods which are able to serve the HTTP requests. 



### Method annotations 
`@RequestMapping` can be used with methods to provide the URI pattern for which handler method will be used.

#### **GET** requests
In Spring, **GET** requests are binded to their handler methods by using `@RequestMapping(value="/",method = RequestMethod.GET)`. The parameter `value` defines the URL path whereas the REST service can be invoked. 
`@GetMapping` is a composed annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.GET).

In order to create RESTful API endpoint, which returns the content of the static list `tasks` defined in `TaskResource`, we can define a find all GET method, registered on the following URL: `http://localhost:8080/tasks/`, by using the following syntax:

```java
@RequestMapping(value="/",method = RequestMethod.GET)
public List<Task> listAll() {
        return tasks;
}
```



### Path Variables
In order to create RESTful API endpoint, which filters a task with specified id in the static list `tasks` defined in `TaskResource`, we can define a getById GET method, registered on the following dynamic URL: `http://localhost:8080/tasks/{id}`, by using the following syntax:

```java
@RequestMapping(value="/{id}",method = RequestMethod.GET)
    public Optional<Task> getById(@PathVariable("id") Long id) {
        return tasks.stream().filter(task -> task.getId().equals(id)).findAny();
    }
```
`@RequestMapping` annotation can be used to handle dynamic URIs where one or more of the URI value works as a parameter. We can even specify Regular Expression for URI dynamic parameter to accept only specific type of input. It works with `@PathVariable` annotation through which we can map the URI variable to one of the method arguments. 

### Request Params

Sometimes we get parameters in the request URL, mostly in GET requests. We can use `@RequestMapping` with `@RequestParam` annotation to retrieve the URL parameter and map it to the method argument. The `required` parameter has the default value `true`, so its value must be specified in the URL as query parameter. To make it optional parameter, the `required` parameter should be `false`. To make  The next code example presents a use case of the @RequestParam in order to filter the list of tasks that contain a the string passed as parameter.

```java
@RequestMapping(value="/query",method = RequestMethod.GET)
    public List<Task> listAll(@RequestParam(value = "name", required = false) String name) {
        if (name!=null) {
            return tasks.stream().filter(task->task.getName().contains(name)).collect(Collectors.toList());
        }
        return tasks;
    }
```

#### **POST** requests with @RequestBody



In order to create RESTful API endpoint, which adds a task to the static list `tasks` defined in `TaskResource`, we can define an add POST method, registered on the following URL: `http://localhost:8080/tasks/`, by using the following syntax:
```java
	@PostMapping(value="/")
    public List<Task> addTask(@RequestBody Task task) {
        tasks.add(task);
        return tasks;
    }
```
`@RequestBody` annotation, used as a parameter in the method `addTask` maps the HttpRequest body to a transfer or domain object, enabling automatic deserialization of the inbound HttpRequest body, in this case JSON object, onto a Java object. It uses Java reflection to convert the incoming JSON request to a Model object. To test this method, you should use the tool for API development and testing [Postman](https://www.getpostman.com/ "Postman").



### Session Attribute

In order to bind a method parameter to a session attribute, we can use annotation @SessionAttribute. In following example, the variable firstName passed as argument in the method getTaskActivities, will be binded with the session attribute firstName. 

```java
@GetMapping("/{index}/activities")
    public List<Activity> getTaskActivities(@PathVariable("index") int index, @RequestParam(required = false) Integer inLastMinutes, @SessionAttribute String firstName) {
        System.out.println("Auth name: " + firstName);
    }
```

### HttpServletRequest 

`HttpServletRequest` extends the ServletRequest interface to provide request information for HTTP servlets. All parameters  the client sends, like query parameters or header values, can be extracted by using `HttpServletRequest`. Also, `HttpServletRequest.getSession()` returns the HttpSession object which provides management of the attributes saved in current HTTP session.


```java
@GetMapping("/getHeader")
    public void getHeaders(HttpServletRequest request) {
        System.out.println("Content-Type Header value:");
        System.out.println(request.getHeader("Content-Type"));
    }

    @GetMapping("/setSessionAttribute")
    public void setSessionAttribute(HttpServletRequest request) {

        request.getSession().setAttribute("customAttribute","My Custom Attribute value");
    }
```


### HttpServletResponse

`HttpServletResponse` extends the ServletResponse interface to provide HTTP-specific functionality in sending a response. For example, it has methods to access HTTP headers and cookies. If we simply have to add the HttpServletResponse object to our REST endpoint as an argument, then use the setHeader() method:

```java
@PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addNew(@RequestBody Task task, HttpServletResponse response) {
        tasks.add(task);
        response.setHeader("Location", "/tasks/" + (tasks.size() - 1));
    }
```
In previous example, `response.setHeader()` method sets the URL location of the newly created `Task` entity.
