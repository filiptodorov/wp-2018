export const getTasksFromApi = () => {
  return fetch('http://localhost:8080/tasks/all');
};

export const createTask = (task) => {
  return fetch('http://localhost:8080/tasks', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      title: task.title,
      project: task.project
    })
  });
};


export const startActivity = (task) => {
  return fetch('http://localhost:8080/tasks/' + task.id, {
    method: 'PUT'
  });
};


export const stopActivity = (task) => {
  return fetch('http://localhost:8080/tasks/' + task.id, {
    method: 'PATCH'
  });
};